# HotOnions - the onion internet hotspot hardware proxy

HotOnions is a project to create secure, tor-encryptet WiFi and ethernet hot spots on a LTE, landline or Satellite connection.

Please note: tis guide is not made for beginners and requires some basic knowledge of Linux and networks.

## The name

- **Hot** - because it creates a hot spot.
- **Onions** - because the whole network traffic is encryptet and routed using the [Tor Project](https://www.torproject.org/)

## Problems using this guide

If you encounter any problems while creating your own HotOnions, open an issue for this project or contact us. We'll try to help you as much as we can.

## Getting started

### Requiremnts

- Raspberry Pi 4 or later with SD-Card, a powersupply etc.
- a LTE/Sattelite/Landline Router or a phone doing tethering
- a WiFi Router to create the WiFi network behind the proxy

### Prepare the SD Card

Download Raspberry Pi OS (previously Raspbian) from [their website](https://www.raspberrypi.org/downloads/raspberry-pi-os/) and set it up. There are various guides in the internet, thet's why we won't explain this again.

Once you have set up your Pi and are logged in to it, open a terminal and run the following commands:

```shell
sudo apt update
sudo apt dist-upgrade

sudo apt install tor torsocks

sudo systemctl enable --now tor.service

sudo nano /etc/tor/torrc

# uncomment the following line:
# SocksPort 9050 # Default: Bind to localhost:9050 for local connections.

# <ctrl>+o
# <enter>
# <ctrl>+x

sudo systemctl restart tor.service

# check whether tor is working
torsocks curl -s https://check.torproject.org | grep -P "to use Tor|not using Tor"
# output should be something similar to
# > Congratulations. This browser is configured to use Tor.

ip a s
```

Now, there should be an output similar to the following:

```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether XX:XX:XX:XX:XX:XX brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.XXX/24 brd 192.168.0.255 scope global dynamic noprefixroute eth0
       valid_lft 85216sec preferred_lft 74416sec
    inet6 XXXX:XXXX:XXXX:XXXX:XXXX:XXXX:XXXX:XXXX/64 scope global dynamic mngtmpaddr noprefixroute
       valid_lft 86166sec preferred_lft 14166sec
    inet6 fe80::8562:e941:7f81:3b0a/64 scope link
       valid_lft forever preferred_lft forever
3: wlan0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether XX:XX:XX:XX:XX:XX brd ff:ff:ff:ff:ff:ff
```

```shell
sudo apt install udhcpd bind9
# now, always replace `eth1` with your network adapters interface name
sudo ifconfig eth1 10.0.42.1
sudo nano /etc/network/interfaces
```

Add the following code at the end and save:

```
auto lo
iface lo inet loopback

allow-hotplug eth0
iface eth0 inet dhcp

allow-hotplug eth1
iface eth1 inet static
    address 10.0.42.1
    netmask 255.255.255.0
```

add the following to `/etc/udhcpd.conf`:

```
start 10.0.42.20
end 10.0.42.254
interface wlan0
remaining yes
opt dns 10.0.42.1
option subnet 255.255.255.0
opt router 10.0.42.1
option lease 864000 # 10 days
```

Add the following to `/etc/default/udhcpd`

```
DHCPD_ENABLED="yes"
```

```shell
sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
```

We want to use adguard dns which is a public dns provider removing advertisments (waste of LTE data). You may use whatever provider you want.

Add the following to `/etc/bind/named.conf.options`

```
options {
    directory "/var/cache/bind";
    forwarders {
        176.103.130.130;
        176.103.130.131;
    };
    dnssec-validation auto;
    auth-nxdomain no; # conform to RFC1035
    listen-on-v6 { any; };
};
```

```shell
echo "net.ipv4.ip_forward=1" | sudo tee -a /etc/sysctl.conf

sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo iptables -A FORWARD -i eth0 -o eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i eth1 -o eth0 -j ACCEPT

sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"

echo "up iptables-restore < /etc/iptables.ipv4.nat" | sudo tee -a /etc/network/interfaces

sudo systemctl enable --now bind9.service
sudo systemctl enable --now udhcpd.service

sudo ip a change dev eth1 10.0.42.1/24
```

Now, route the incomming network traffic to tor

Add the following to `/etc/tor/torrc`:

```
VirtualAddrNetwork 10.192.0.0/10
AutomapHostsSuffixes .onion,.exit
AutomapHostsOnResolve 1
TransPort 9040
TransListenAddress 10.0.42.1
DNSPort 53
DNSListenAddress 10.0.42.1
```

```shell
sudo systemctl restart tor.service

sudo iptables -F
sudo iptables -t nat -F

sudo iptables-restore /etc/iptables.ipv4.nat

sudo iptables -t nat -A PREROUTING -i eth1 -p tcp --dport 22 -j REDIRECT --to-ports 22
sudo iptables -t nat -A PREROUTING -i eth1 -p udp --dport 53 -j REDIRECT --to-ports 53
sudo iptables -t nat -A PREROUTING -i eth1 -p tcp --syn -j REDIRECT --to-ports 9040

sudo sh -c "iptables-save > /etc/iptables.ipv4.tor.nat"
```
